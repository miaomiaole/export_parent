package cn.itcast.controller.stat;

import cn.itcast.controller.BaseController;
import cn.itcast.domain.system.User;
import cn.itcast.service.stat.StatService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/stat")
public class StatController extends BaseController{

    // 注入service
    @Reference
    private StatService statService;

    /**
     * 访问统计分析三个模块：
     * http://localhost:8080/stat/toCharts.do?chartsType=factory
     * http://localhost:8080/stat/toCharts.do?chartsType=sell
     * http://localhost:8080/stat/toCharts.do?chartsType=online
     */
    @RequestMapping("/toCharts")
    public String toCharts(String chartsType){
        return "stat/stat-" + chartsType;
    }

    /**
     * 1. 根据生产厂家销售统计
     */
    @RequestMapping("/getFactoryDate")
    @ResponseBody
    public List<Map<String, Object>> getFactoryDate(){
        List<Map<String, Object>> list = statService.getFactoryData(getLoginCompanyId());
        return list;
    }

    /**
     * 2. 产品销售量要求按前5名统计
     */
    @RequestMapping("/getSellData")
    @ResponseBody
    public List<Map<String, Object>> getSellData(){
        List<Map<String, Object>> list = statService.getSellData(getLoginCompanyId());
        return list;
    }

    /**
     * 3. 根据小时统计访问人数
     */
    @RequestMapping("/getOnlineData")
    @ResponseBody
    public List<Map<String, Object>> getOnlineData(){
        List<Map<String, Object>> list = statService.getOnlineData(getLoginCompanyId());
        return list;
    }
}

















