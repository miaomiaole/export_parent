package cn.itcast.controller.cargo;

import cn.itcast.controller.BaseController;
import cn.itcast.domain.cargo.*;
import cn.itcast.service.cargo.ContractService;
import cn.itcast.service.cargo.ExportProductService;
import cn.itcast.service.cargo.ExportService;
import cn.itcast.vo.ExportProductVo;
import cn.itcast.vo.ExportResult;
import cn.itcast.vo.ExportVo;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/cargo/export")
public class ExportController extends BaseController {

    // 注入购销合同service
    @Reference
    private ContractService contractService;
    @Reference(retries = 0)
    private ExportService exportService;
    @Reference
    private ExportProductService exportProductService;


    /**
     * 1. 合同管理列表
     * 功能入口： 点击合同管理
     * 访问地址： http://localhost:8080/cargo/export/contractList.do
     * 响应地址： /WEB-INF/pages/cargo/export/export-contractList.jsp
     */
    @RequestMapping("/contractList")
    public String contractList(
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "5") int pageSize){
        // 查询状态为1的购销合同  (0草稿 1已上报待报运	2 已报运)
        ContractExample contractExample = new ContractExample();
        contractExample.createCriteria().andStateEqualTo(1);
        PageInfo<Contract> pageInfo =
                contractService.findByPage(contractExample, pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);
        return "cargo/export/export-contractList";
    }

    /**
     * 2. 购销合同列表
     * 访问地址：http://localhost:8080/cargo/export/list.do
     * 响应地址： /WEB-INF/pages/cargo/export/export-list.jsp
     */
    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") int pageNum,
                       @RequestParam(defaultValue = "5") int pageSize){

        // 根据登陆用户的企业id查询
        ExportExample exportExample = new ExportExample();
        exportExample.createCriteria().andCompanyIdEqualTo(getLoginCompanyId());
        PageInfo<Export> pageInfo =
                exportService.findByPage(exportExample, pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);
        return "cargo/export/export-list";
    }

    /**
     * 3. 在合同管理列表页面，选中多个购销合同，点击报运，进入报运的添加页面。
     * 需求：实现进入报运添加页面
     * 功能入口： 合同管理的“报运”按钮
     * 请求地址： http://localhost:8080/cargo/export/toExport.do
     * 请求参数： id=1   id=2
     * 后台封装数据
     *      1. public String toExport(String[] id){}
     *      2. public String toExport(String id){}   多个id自动用逗号隔开（http协议）
     *  响应地址：/WEB-INF/pages/cargo/export/export-toExport.jsp
     */
    @RequestMapping("/toExport")
    public String toExport(String id){
        // 保存多个购销合同id
        request.setAttribute("id",id);
        // 进入出口报运的添加页面
        return "cargo/export/export-toExport";
    }

    /**
     * 4. 出口报运单添加或者修改方法
     */
    @RequestMapping("/edit")
    public String edit(Export export) throws Exception {
        export.setCompanyId(getLoginCompanyId());
        export.setCompanyName(getLoginCompanyName());

        // 3.1 根据主键id判断添加或者修改
        if (StringUtils.isEmpty(export.getId())){
            exportService.save(export);
        } else {
            // 3.2 执行修改操作
            exportService.update(export);
        }
        // 3.3 操作成功，返回到购销合同列表
        return "redirect:/cargo/export/list.do";
    }

    /**
     * 5. 进入修改页面
     * 请求地址：http://localhost:8080/cargo/export/toUpdate.do?id=2
     * 响应地址：/WEB-INF/pages/cargo/export/export-update.jsp
     */
    @RequestMapping("/toUpdate")
    public String toUpdate(String id){
        //1. 查询报运单
        Export export = exportService.findById(id);
        request.setAttribute("export",export);

        //2. 查询报运的商品
        ExportProductExample example = new ExportProductExample();
        example.createCriteria().andExportIdEqualTo(id);
        List<ExportProduct> list = exportProductService.findAll(example);
        request.setAttribute("eps",list);
        return "cargo/export/export-update";
    }

    /**
     * 6. 提交与取消
     */
    @RequestMapping("/submit")
    public String submit(String id) throws Exception {
        // 根据报运单id查询
        Export export = new Export();
        export.setId(id);
        export.setState(1);
        // 更新
        exportService.update(export);
        return "redirect:/cargo/export/list.do";
    }

    @RequestMapping("/cancel")
    public String cancel(String id) throws Exception {
        // 根据报运单id查询
        Export export = new Export();
        export.setId(id);
        export.setState(0);
        // 更新
        exportService.update(export);
        return "redirect:/cargo/export/list.do";
    }

    /**
     * 7. 电子报运，远程调用海关平台的webservice服务端。
     * 后台请求：
     *      地址：http://localhost:8080/cargo/export/exportE.do?id=1
     * 海关平台保存报运结果：
     *      地址： http://localhost:9001/ws/export/user
     *      提交方式： post
     *      请求参数： ExportVo
     * 查询报运结果：
     *      地址：http://localhost:9001/ws/export/user/报运单id
     *      提交方式： get
     *      请求参数： 报运单id
     *      响应数据： ExportResultVo
     */
    @RequestMapping("/exportE")
    public String exportE(String id) throws Exception {
        // 根据报运单id查询
        Export export = exportService.findById(id);
        
        // 根据报运单的id查询所有商品
        ExportProductExample epExample = new ExportProductExample();
        epExample.createCriteria().andExportIdEqualTo(id);
        List<ExportProduct> exportProductList = exportProductService.findAll(epExample);

        // 构造ExportVo，封装webservice请求的参数
        ExportVo exportVo = new ExportVo();
        // 把export--->exportVo
        BeanUtils.copyProperties(export,exportVo);
        // 设置报运单的id
        exportVo.setExportId(id);

        // 封装电子报运的商品信息
        List<ExportProductVo> products = exportVo.getProducts();
        // 遍历报运单的商品
        if (exportProductList != null && exportProductList.size()>0){
            for (ExportProduct ep : exportProductList){
                // 创建电子报运的商品
                ExportProductVo epVo = new ExportProductVo();
                // 拷贝ep ---> epVo
                BeanUtils.copyProperties(ep,epVo);
                // 设置报运单id
                epVo.setExportId(id);
                // 设置报运单商品id
                epVo.setExportProductId(ep.getId());

                // 添加电子报运的商品到集合
                products.add(epVo);
            }
        }
        //exportVo.setProducts(products);

        // 电子报运（1）调用海关平台保存报运单到海关数据库
        WebClient
                .create("http://localhost:9001/ws/export/user")
                .post(exportVo);
        // 电子报运（2）查询报运结果
        ExportResult exportResult =
                WebClient
                    .create("http://localhost:9001/ws/export/user/" + id)
                    .get(ExportResult.class);

        // 根据电子报运结果，修改后台项目的数据库：
        exportService.exportE(exportResult);
        return "redirect:/cargo/export/list.do";
    }

}
