package cn.itcast.controller.cargo;

import cn.itcast.controller.BaseController;
import cn.itcast.controller.utils.FileUploadUtil;
import cn.itcast.domain.cargo.*;
import cn.itcast.service.cargo.ContractProductService;
import cn.itcast.service.cargo.ExtCproductService;
import cn.itcast.service.cargo.FactoryService;
import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/cargo/extCproduct")
public class ExtCproductController extends BaseController {

    // 注入service:import com.alibaba.dubbo.config.annotation.Reference;
    @Reference
    private FactoryService factoryService;
    // 注入附件service
    @Reference(retries = 0)
    private ExtCproductService extCproductService;

    /**
     * 1.货物列表，点击附件
     * 请求地址：http://xx:8080/cargo/extCproduct/list.do?contractId=2&contractProductId=2
     * 响应地址：/WEB-INF/pages/cargo/extc/extc-list.jsp
     */
    @RequestMapping("/list")
    public String list(
            String contractId,String contractProductId,
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "5") int pageSize) {

        //1.1 查询附件的工厂
        FactoryExample factoryExample = new FactoryExample();
        factoryExample.createCriteria().andCtypeEqualTo("附件");
        List<Factory> factoryList = factoryService.findAll(factoryExample);
        request.setAttribute("factoryList", factoryList);

        //1.2 查询货物下的附件
        ExtCproductExample extCproductExample = new ExtCproductExample();
        extCproductExample.createCriteria().andContractProductIdEqualTo(contractProductId);
        PageInfo<ExtCproduct> pageInfo =
                extCproductService.findByPage(extCproductExample, pageNum, pageSize);
        request.setAttribute("pageInfo",pageInfo);

        //1.3 存储货物、购销合同的id
        request.setAttribute("contractProductId",contractProductId);
        request.setAttribute("contractId",contractId);
        return "cargo/extc/extc-list";
    }

    /**
     * 2. 添加附件/修改附件
     */
    @RequestMapping("/edit")
    public String edit(ExtCproduct extCproduct) throws Exception {
        //根据主键id判断添加或者修改
        if (StringUtils.isEmpty(extCproduct.getId())){
            extCproductService.save(extCproduct);
        } else {
            extCproductService.update(extCproduct);
        }
        //操作成功，返回到附件列表
        return "redirect:/cargo/extCproduct/list.do?contractId=" + extCproduct.getContractId()
                    + "&contractProductId=" + extCproduct.getContractProductId();
    }

    /**
     * 3. 进入修改页面
     */
    @RequestMapping("/toUpdate")
    public String toUpdate(String id,String contractId,String contractProductId){
        // 查询附件的工厂
        FactoryExample factoryExample = new FactoryExample();
        factoryExample.createCriteria().andCtypeEqualTo("附件");
        List<Factory> factoryList = factoryService.findAll(factoryExample);
        request.setAttribute("factoryList",factoryList);

        // 根据附件id查询
        ExtCproduct extCproduct = extCproductService.findById(id);
        request.setAttribute("extCproduct",extCproduct);

        // 保存购销合同id、货物id
        request.setAttribute("contractId",contractId);
        request.setAttribute("contractProductId",contractProductId);

        return "cargo/extc/extc-update";
    }

    /**
     * 4. 删除
     */
    @RequestMapping("/delete")
    public String delete(String id,String contractId,String contractProductId){
        //调用service删除
        extCproductService.delete(id);
        //操作成功，返回到附件列表
        return "redirect:/cargo/extCproduct/list.do?contractId=" + contractId
                + "&contractProductId=" + contractProductId;
    }

}