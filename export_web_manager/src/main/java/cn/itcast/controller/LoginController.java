package cn.itcast.controller;

import cn.itcast.domain.system.Module;
import cn.itcast.domain.system.User;
import cn.itcast.service.system.ModuleService;
import cn.itcast.service.system.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Security;
import java.util.List;

@Controller
public class LoginController extends BaseController{

    @Autowired
    private UserService userService;
    @Autowired
    private ModuleService moduleService;

    /**
     * 登陆认证(1)传统方式实现
     * 登陆访问页面
     * 1. 访问：http://lcoalhost:8080/index.jsp
     * 2. index.jsp ----> window.location.href = "login.do";
     * 3. 所以，这里定义了LoginController.login()方法。
     * 4. login() ---> /home/main.jsp

    @RequestMapping("/login")
    public String login(String email,String password){
        // 1. 登陆请求参数校验：email 表示邮箱； password表示密码
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)){
            // 跳转到登陆页面
            return "forward:/login.jsp";
        }

        // 2. 登陆验证
        // 2.1 调用service，根据邮箱查询用户是否存在。注意：email要唯一。
        User user = userService.findByEmail(email);
        // 2.2 判断
        if (user != null && user.getPassword().equals(password)){
            // 2.3 登陆认证成功,跳转到main.jsp页面
            session.setAttribute("loginUser",user);
            // 2.3 根据登陆用户id，查询权限
            List<Module> moduleList = moduleService.findModulesByUserId(user.getId());
            // 2.3 存储到session
            session.setAttribute("moduleList",moduleList);
            return "home/main";
        } else {
            // 2.3 登陆失败
            request.setAttribute("error","用户名或者密码错误！");
            return "forward:/login.jsp";
        }
    }
     */

    /**
     * 登陆认证(2)shiro方式实现
     */
    @RequestMapping("/login")
    public String login(String email,String password){
        // 1. 登陆请求参数校验：email 表示邮箱； password表示密码
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)){
            // 跳转到登陆页面
            return "forward:/login.jsp";
        }

        try {
            // 2. 登陆验证
            // 2.1 创建subject对象
            Subject subject = SecurityUtils.getSubject();
            // 2.2 创建shiro提供的封装账号密码的token对象
            UsernamePasswordToken token =
                    new UsernamePasswordToken(email,password);
            // 2.3 登陆认证, 去到realm中查询 -------> [关键代码]
            subject.login(token);
            // 2.4 认证成功,获取认证的身份对象
            User user = (User) subject.getPrincipal();
            // 2.5 存储登陆用户到session
            session.setAttribute("loginUser",user);
            // 2.6 根据登陆用户id，查询权限, 保存用户权限
            List<Module> moduleList = moduleService.findModulesByUserId(user.getId());
            session.setAttribute("moduleList",moduleList);
            return "home/main";
        } catch (Exception e) {
            e.printStackTrace();
            // 登陆失败
            request.setAttribute("error","用户名或者密码错误！");
            return "forward:/login.jsp";
        }
    }

    /**
     * 显示主页
     * 5. /home/main.jsp --->src="/home.do"
     */
    @RequestMapping("/home")
    public String home(){
        return "home/home";
    }

    /**
     * 注销，实现登陆用户的退出.
     */
    @RequestMapping("/logout")
    public String logout(){
        // shiro也提供了退出方法(清除shiro的认证信息)
        SecurityUtils.getSubject().logout();
        // 先清空session中登陆用户
        session.removeAttribute("loginUser");
        // 销毁服务端session
        session.invalidate();
        return "forward:/login.jsp";
    }

}



















