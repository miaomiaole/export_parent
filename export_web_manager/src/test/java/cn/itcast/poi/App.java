package cn.itcast.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class App {
    // 生层excel
    @Test
    public void write() throws Exception {
        // 1. 创建工作簿
        Workbook workbook = new XSSFWorkbook();
        // 2. 创建工作表
        Sheet sheet = workbook.createSheet("测试");
        // 3. 创建第一行
        Row row = sheet.createRow(0);
        // 4. 创建第一行的第一列
        Cell cell = row.createCell(0);
        // 5. 设置单元格内容
        cell.setCellValue("第一行第一列");

        // 6. 输出到本地磁盘
        workbook.write(new FileOutputStream("e:\\test.xlsx"));
        workbook.close();
    }

    // 读取
    @Test
    public void read() throws Exception {
        // 1. 创建工作簿
        Workbook workbook = new XSSFWorkbook(new FileInputStream("e:\\test.xlsx"));
        // 2. 获取工作簿
        Sheet sheet = workbook.getSheetAt(0);
        // 3. 获取行
        Row row = sheet.getRow(0);
        // 4. 获取单元格
        Cell cell = row.getCell(0);
        // 5. 获取单元格内容
        System.out.println("单元格内容" + cell.getStringCellValue());
        System.out.println("获取总行数" + sheet.getPhysicalNumberOfRows());
        System.out.println("获取总列数" + row.getPhysicalNumberOfCells());


        workbook.close();
    }

}
