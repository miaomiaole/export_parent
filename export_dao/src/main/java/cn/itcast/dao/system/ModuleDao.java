package cn.itcast.dao.system;

import cn.itcast.domain.system.Module;

import java.util.List;

public interface ModuleDao {

    //查询全部
    List<Module> findAll();

    //根据id查询
    Module findById(String moduleId);

    //根据id删除
    void delete(String moduleId);

    //添加
    void save(Module module);

    //更新
    void update(Module module);

    List<Module> findModulesByRoleId(String roleId);

    // 根据用户级别查询不同的权限
    List<Module> findByBelong(int belong);

    //根据登陆用户id，查询权限
    List<Module> findModulesByUserId(String userId);
}