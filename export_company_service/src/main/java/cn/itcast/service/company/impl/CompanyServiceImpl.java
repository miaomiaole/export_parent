package cn.itcast.service.company.impl;

import cn.itcast.dao.company.CompanyDao;
import cn.itcast.domain.company.Company;
import cn.itcast.service.company.CompanyService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.UUID;

// 注意：要引入dubbo的注解
// import com.alibaba.dubbo.config.annotation.Service;
@Service
public class CompanyServiceImpl implements CompanyService {

    // 注入dao
    @Autowired
    private CompanyDao companyDao;

    @Override
    public List<Company> findAll() {
        return companyDao.findAll();
    }

    @Override
    public void save(Company company) {
        // 把uuid作为主键
        company.setId(UUID.randomUUID().toString());
        companyDao.save(company);
    }

    @Override
    public void update(Company company) {
        companyDao.update(company);
    }

    @Override
    public Company findById(String id) {
        return companyDao.findById(id);
    }

    @Override
    public void delete(String id) {
        companyDao.delete(id);
    }

    @Override
    public PageInfo<Company> findByPage(int pageNum, int pageSize) {
        //1. 开始分页。会对其后的第一条select查询语句进行分页
        PageHelper.startPage(pageNum,pageSize);
        //2. 调用dao方法进行查询
        List<Company> list = companyDao.findAll();
        //3. 返回PageHelper提供的封装分页参数的对象
        PageInfo<Company> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}
