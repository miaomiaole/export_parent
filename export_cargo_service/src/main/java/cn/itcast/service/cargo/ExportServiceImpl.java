package cn.itcast.service.cargo;

import cn.itcast.dao.cargo.*;
import cn.itcast.domain.cargo.*;
import cn.itcast.vo.ExportProductResult;
import cn.itcast.vo.ExportResult;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

// import com.alibaba.dubbo.config.annotation.Service;
@Service(timeout = 100000)
public class ExportServiceImpl implements ExportService {

    // 注入dao
    @Autowired
    private ExportDao exportDao;
    // 购销合同
    @Autowired
    private ContractDao contractDao;
    // 货物
    @Autowired
    private ContractProductDao contractProductDao;
    // 购销合同附件
    @Autowired
    private ExtCproductDao extCproductDao;
    // 报运商品
    @Autowired
    private ExportProductDao exportProductDao;
    // 商品附件
    @Autowired
    private ExtEproductDao extEproductDao;

    @Override
    public PageInfo<Export> findByPage(
            ExportExample exportExample, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Export> list = exportDao.selectByExample(exportExample);
        return new PageInfo<>(list);
    }

    @Override
    public Export findById(String id) {
        return exportDao.selectByPrimaryKey(id);
    }

    /**
     * 已知条件：购销合同ids, 多个用逗号隔开
     * 需求：
     *      1. 添加报运单     ----->  设置参数
     *      2. 添加报运商品   ----->  根据购销合同id查询商品
     *      3. 添加商品附件   ----->  根据购销合同id查询附件
     */
    @Override
    public void save(Export export) {
        //1. 设置报运单的参数： id、制单时间、创建时间、状态
        export.setId(UUID.randomUUID().toString());
        export.setInputDate(new Date());
        export.setCreateTime(new Date());
        export.setState(0);  // 草稿

        //2. 获取购销合同的ids
        String contractIds = export.getContractIds();

        //3. 查询购销合同，修改购销合同的状态、获取合同号
        String contractNo = "";
        String[] array = contractIds.split(",");
        for (String contractId : array) {
            // 查询购销合同
            Contract contract = contractDao.selectByPrimaryKey(contractId);
            // 修改购销合同的状态: 2
            contract.setState(2);
            // 修改购销合同
            contractDao.updateByPrimaryKeySelective(contract);
            // 获取合同号, 多个合同号用空格隔开
            contractNo += contract.getContractNo() + " ";

        }
        // 设置出口报运的合同号
        export.setCustomerContract(contractNo);

        //4. 处理出口报运的商品:  根据购销合同查询所有货物---> 数据搬家到商品表
        // 根据购销合同ids，查询所有货物
        ContractProductExample cpExample = new ContractProductExample();
        cpExample.createCriteria().andContractIdIn(Arrays.asList(array));
        List<ContractProduct> cpList = contractProductDao.selectByExample(cpExample);

        // 定义map，key是货物id，value就是商品id
        Map<String,String> map = new HashMap<>();

        // 货物 ---> 商品
        for (ContractProduct cp : cpList) {
            // 创建商品
            ExportProduct ep = new ExportProduct();
            // 把cp对象的数据，拷贝到ep对象中
            BeanUtils.copyProperties(cp,ep);
            // 封装商品对象（1） 设置id
            ep.setId(UUID.randomUUID().toString());
            // 封装商品对象（1） 设置出口报运单id
            ep.setExportId(export.getId());
            // 保存商品
            exportProductDao.insertSelective(ep);

            // 设置保存货物id，以及对应的商品id
            map.put(cp.getId(),ep.getId());
        }

        //5. 处理出口报运的附件：  根据购销合同查询所有附件---> 数据搬家到商品附件表
        // 根据购销合同查询所有附件
        ExtCproductExample extcExample = new ExtCproductExample();
        extcExample.createCriteria().andContractIdIn(Arrays.asList(array));
        List<ExtCproduct> extcList = extCproductDao.selectByExample(extcExample);
        // 货物附件 ---> 商品附件
        for (ExtCproduct extcProduct : extcList) {
            // 创建商品附件对象
            ExtEproduct exteProduct = new ExtEproduct();
            // 对象拷贝
            BeanUtils.copyProperties(extcProduct,exteProduct);
            // 封装商品附件（1）id
            exteProduct.setId(UUID.randomUUID().toString());
            // 封装商品附件（2）设置报运单id
            exteProduct.setExportId(export.getId());
            /**
             * 封装商品附件（3）封装商品货物id
             * 如何拿到商品货物ID？
             *   1. 已知条件：货物id。  extcProduct.getContractProductId()
             *   2. 求商品附件中的商品ID
             *   3. 定义集合封装货物ID与商品ID： Map<货物ID，商品ID>
             */
            // 获取出口报运单下附件的商品的id
            String exportProductId = map.get(extcProduct.getContractProductId());
            exteProduct.setExportProductId(exportProductId);

            // 保存附件
            extEproductDao.insertSelective(exteProduct);
        }


        // 设置报运单的商品数量
        export.setProNum(cpList.size());
        // 设置报运单的商品附件数量
        export.setExtNum(extcList.size());

        //6. 保存报运单
        exportDao.insertSelective(export);
    }

    @Override
    public void update(Export export) {
        //1. 修改报运单信息
        exportDao.updateByPrimaryKeySelective(export);

        //2. 修改报运单下的商品（长宽高...）
        List<ExportProduct> epList = export.getExportProducts();
        if (epList != null && epList.size()>0){
            for (ExportProduct exportProduct : epList) {
                //调用dao实现修改商品
                exportProductDao.updateByPrimaryKeySelective(exportProduct);
            }
        }
    }

    @Override
    public void delete(String id) {
        exportDao.deleteByPrimaryKey(id);
    }


    @Override
    public void exportE(ExportResult exportResult) {
        //1. 获取报运单id
        String exportId = exportResult.getExportId();

        //2. 修改报运单信息
        Export export = exportDao.selectByPrimaryKey(exportId);
        export.setState(exportResult.getState());
        export.setRemark(exportResult.getRemark());
        exportDao.updateByPrimaryKeySelective(export);

        //3. 获取电子报运返回的商品信息
        Set<ExportProductResult> products = exportResult.getProducts();
        if (products != null && products.size()>0){
            for (ExportProductResult product : products) {
                // 根据商品id查询
                ExportProduct ep =
                        exportProductDao.selectByPrimaryKey(product.getExportProductId());
                // 设置交税金额
                ep.setTax(product.getTax());
                // 修改商品
                exportProductDao.updateByPrimaryKeySelective(ep);
            }
        }

    }


}













