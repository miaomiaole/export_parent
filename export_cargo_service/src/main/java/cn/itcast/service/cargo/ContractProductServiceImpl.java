package cn.itcast.service.cargo;

import cn.itcast.dao.cargo.ContractDao;
import cn.itcast.dao.cargo.ContractProductDao;
import cn.itcast.dao.cargo.ExtCproductDao;
import cn.itcast.domain.cargo.*;
import cn.itcast.vo.ContractProductVo;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Service
public class ContractProductServiceImpl implements ContractProductService {

    @Autowired
    private ContractProductDao contractProductDao;
    @Autowired
    private ContractDao contractDao;
    @Autowired
    private ExtCproductDao extCproductDao;

    @Override
    public PageInfo<ContractProduct> findByPage(ContractProductExample cpExample, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(contractProductDao.selectByExample(cpExample));
    }

    @Override
    public List<ContractProduct> findAll(ContractProductExample contractProductExample) {
        return contractProductDao.selectByExample(contractProductExample);
    }

    @Override
    public ContractProduct findById(String id) {
        return contractProductDao.selectByPrimaryKey(id);
    }

    @Override
    public void save(ContractProduct contractProduct) {
        //1. 计算货物的金额 = 单价 * 数量
        Double amount = 0d;
        if (contractProduct.getPrice() != null && contractProduct.getCnumber()!=null) {
            amount = contractProduct.getPrice() * contractProduct.getCnumber();
        }
        //2. 设置货物金额
        contractProduct.setAmount(amount);

        //3. 保存货物
        contractProduct.setId(UUID.randomUUID().toString());
        contractProductDao.insertSelective(contractProduct);

        //4. 修改购销合同的总金额 += 货物金额
        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());
        contract.setTotalAmount(contract.getTotalAmount() + amount);
        contract.setProNum(contract.getProNum() + 1);
        //5. 修改购销合同
        contractDao.updateByPrimaryKeySelective(contract);
    }

    @Override
    public void update(ContractProduct contractProduct) {

        //根据货物id查询，查询获取修改前的货物进行
        ContractProduct cp =
                contractProductDao.selectByPrimaryKey(contractProduct.getId());
        Double oldAmount = cp.getAmount();

        //计算修改后的货物金额
        Double amount = 0d;
        if (contractProduct.getPrice() != null && contractProduct.getCnumber()!=null) {
            amount = contractProduct.getPrice() * contractProduct.getCnumber();
        }
        contractProduct.setAmount(amount);

        //修改货物
        contractProductDao.updateByPrimaryKeySelective(contractProduct);

        //根据购销合同id查询购销合同
        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());

        //修改购销合同总金额
        contract.setTotalAmount(contract.getTotalAmount() + amount - oldAmount);

        //调用购销合同service，修改购销合同
        contractDao.updateByPrimaryKeySelective(contract);
    }

    // 删除货物
    @Override
    public void delete(String id) {
        //1. 根据货物id查询
        ContractProduct contractProduct = contractProductDao.selectByPrimaryKey(id);

        //2. 获取要删除的货物的金额
        double productAmount = contractProduct.getAmount();

        //3. 根据货物的id查询附件, 累加附件总金额、删除附件
        ExtCproductExample example = new ExtCproductExample();
        example.createCriteria().andContractProductIdEqualTo(id);
        List<ExtCproduct> list = extCproductDao.selectByExample(example);
        double extcAmount = 0d;
        if (list != null && list.size()>0) {
            for (ExtCproduct extCproduct : list) {
                // 累加附件总金额
                extcAmount += extCproduct.getAmount();
                // 删除附件
                extCproductDao.deleteByPrimaryKey(extCproduct.getId());
            }
        }

        //4. 删除货物
        contractProductDao.deleteByPrimaryKey(id);

        //5. 修改购销合同总金额 = 销合同总金额 - 货物  - 附件
        Contract contract = contractDao.selectByPrimaryKey(contractProduct.getContractId());
        // 设置购销合同总金额
        contract.setTotalAmount(contract.getTotalAmount() - productAmount - extcAmount);
        // 购销合同中货物数量 -1
        contract.setProNum(contract.getProNum() - 1);
        // 购销合同中附件数量 -list.size();
        contract.setExtNum(contract.getExtNum() - list.size());
        // 修改购销合同
        contractDao.updateByPrimaryKeySelective(contract);
    }

    @Override
    public List<ContractProductVo> findByShipTime(String shipTime, String companyId) {
        return contractProductDao.findByShipTime(shipTime,companyId);
    }
}
