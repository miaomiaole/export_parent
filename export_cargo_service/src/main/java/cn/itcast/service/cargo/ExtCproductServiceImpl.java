package cn.itcast.service.cargo;

import cn.itcast.dao.cargo.ContractDao;
import cn.itcast.dao.cargo.ExtCproductDao;
import cn.itcast.domain.cargo.Contract;
import cn.itcast.domain.cargo.ExtCproduct;
import cn.itcast.domain.cargo.ExtCproductExample;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Service(timeout = 1000000)
public class ExtCproductServiceImpl implements ExtCproductService {

    @Autowired
    private ContractDao contractDao;
    @Autowired
    private ExtCproductDao extCproductDao;

    @Override
    public PageInfo<ExtCproduct> findByPage(
            ExtCproductExample extCproductExample, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return new PageInfo<>(extCproductDao.selectByExample(extCproductExample));
    }

    @Override
    public List<ExtCproduct> findAll(ExtCproductExample extCproductExample) {
        return extCproductDao.selectByExample(extCproductExample);
    }

    @Override
    public ExtCproduct findById(String id) {
        return extCproductDao.selectByPrimaryKey(id);
    }

    // 保存附件
    @Override
    public void save(ExtCproduct extCproduct) {
        //1. 设置id
        extCproduct.setId(UUID.randomUUID().toString());

        //2. 计算用户输入的附件金额 = 单价 * 数量
        double amount=0d;
        if (extCproduct.getPrice() != null && extCproduct.getCnumber() != null) {
            amount = extCproduct.getPrice() * extCproduct.getCnumber();
        }

        //3. 设置附件金额
        extCproduct.setAmount(amount);

        //4. 保存附件
        extCproductDao.insertSelective(extCproduct);

        //5. 修改购销合同总金额（1）根据附件中的购销合同id查询
        Contract contract =
                contractDao.selectByPrimaryKey(extCproduct.getContractId());

        //6. 修改购销合同总金额（2）设置购销合同总金额=总金额+附件金额
        contract.setTotalAmount(contract.getTotalAmount()+amount);
        // 修改附件数量
        contract.setExtNum(contract.getExtNum() + 1);

        //7. 修改购销合同总金额（3）修改购销合同
        contractDao.updateByPrimaryKeySelective(contract);
    }

    @Override
    public void update(ExtCproduct extCproduct) {
        //1. 先根据购销合同id查询
        Contract contract = contractDao.selectByPrimaryKey(extCproduct.getContractId());

        //2. 根据附件id，查询附件，获取修改前的附件总金额
        ExtCproduct extC = extCproductDao.selectByPrimaryKey(extCproduct.getId());

        //3. 获取修改前附件的总金额
        double oldAmount = extC.getAmount();

        //4. 计算修改后附件的总金额
        double amount = 0d;
        if (extCproduct.getPrice() != null && extCproduct.getCnumber() != null) {
            amount = extCproduct.getPrice() * extCproduct.getCnumber();
        }

        //5. 设置修改后的附件金额
        extCproduct.setAmount(amount);

        //6. 修改附件
        extCproductDao.updateByPrimaryKeySelective(extCproduct);

        //7. 修改购销合同总金额(1) 设置总金额
        contract.setTotalAmount(contract.getTotalAmount() + amount - oldAmount);

        //8. 修改购销合同总金额(2) 修改
        contractDao.updateByPrimaryKeySelective(contract);

    }

    // 删除附件
    @Override
    public void delete(String id) {
        //1. 根据附件id查询
        ExtCproduct extCproduct = extCproductDao.selectByPrimaryKey(id);

        //2.获取删除的附件的金额
        double amount = extCproduct.getAmount();

        //3. 删除附件
        extCproductDao.deleteByPrimaryKey(id);

        //4. 根据购销合同的id查询
        Contract contract =
                contractDao.selectByPrimaryKey(extCproduct.getContractId());
        //5. 设置购销合同总金额
        contract.setTotalAmount(contract.getTotalAmount() - amount);
        //6. 设置购销合同的附件数量减1
        contract.setExtNum(contract.getExtNum()-1);
        //7. 修改购销合同
        contractDao.updateByPrimaryKeySelective(contract);
    }
}



















