package cn.itcast.service.system.impl;

import cn.itcast.dao.system.ModuleDao;
import cn.itcast.dao.system.UserDao;
import cn.itcast.domain.system.Module;
import cn.itcast.domain.system.User;
import cn.itcast.service.system.ModuleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ModuleServiceImpl implements ModuleService{
    // 注入dao
    @Autowired
    private ModuleDao moduleDao;
    @Autowired
    private UserDao userDao;

    @Override
    public PageInfo<Module> findByPage(int pageNum, int pageSize) {
        // 开启分页
        PageHelper.startPage(pageNum,pageSize);
        // 调用dao查询，并且封装分页参数返回
        return new PageInfo<>(moduleDao.findAll());
    }

    @Override
    public Module findById(String id) {
        return moduleDao.findById(id);
    }

    @Override
    public void save(Module module) {
        // 设置id值
        module.setId(UUID.randomUUID().toString());
        moduleDao.save(module);
    }

    @Override
    public void update(Module module) {
        moduleDao.update(module);
    }

    @Override
    public List<Module> findAll() {
        return moduleDao.findAll();
    }

    @Override
    public List<Module> findModulesByRoleId(String roleId) {
        return moduleDao.findModulesByRoleId(roleId);
    }

    /**
     * 根据登陆用户id，查询权限
     * 1. 根据用户id，查询用户。
     * 2. 获取用户的级别degree
     * 3. 根据用户的degree进行判断
     *    degree=0  SAAS管理员，可以查看哪些模块？SAAS模块（企业、模块） belong=0
     *               SELECT * FROM ss_module WHERE belong=0
     *    degree=1  企业管理员， 可以查看哪些模块？在Module对应的表中的belong=1
     *               SELECT * FROM ss_module WHERE belong=1
     *    degree=其他 根据用户id查询权限。（用户角色表、角色权限表、权限表）
     */
    @Override
    public List<Module> findModulesByUserId(String id) {
        //1. 根据用户id，查询用户。
        User user =  userDao.findById(id);
        //2. 获取用户的级别degree, 判断
        if (user.getDegree() == 0){
            // SAAS管理员
            return moduleDao.findByBelong(0);
        }
        else if (user.getDegree() == 1){
            // 企业管理员
            return moduleDao.findByBelong(1);
        }
        else {
            return moduleDao.findModulesByUserId(id);
        }
    }

    @Override
    public void delete(String id) {
        moduleDao.delete(id);
    }
}
