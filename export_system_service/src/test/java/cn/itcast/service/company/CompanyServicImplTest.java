package cn.itcast.service.company;

import cn.itcast.domain.company.Company;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
/*
@ContextConfiguration(
        {"classpath:spring/applicationContext-dao.xml","classpath:spring/applicationContext-tx.xml"})
*/

// classpath* 表示加载所有项目类路径下的指定配置文件。
@ContextConfiguration("classpath*:spring/applicationContext-*.xml")
public class CompanyServicImplTest {

    //// 注入service
    //@Autowired
    //private CompanyService companyService;
//
    //@Test
    //public void findByPage(){
    //    PageInfo<Company> pageInfo = companyService.findByPage(1, 2);
    //    System.out.println(pageInfo);
    //}
}
