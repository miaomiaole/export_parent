package cn.itcast.service.stat;

import java.util.List;
import java.util.Map;

public interface StatService {
    /**
     * 1) 生产厂家销售统计
     */
    List<Map<String,Object>> getFactoryData(String companyId);


    /**
     * 2）产品销售量要求按前5名统计
     */
    List<Map<String,Object>> getSellData(String companyId);

    /**
     * 3) 按小时统计访问人数
     */
    List<Map<String,Object>> getOnlineData(String companyId);
}
